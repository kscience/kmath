# Module kmath-memory



## Usage

## Artifact:

The Maven coordinates of this project are `space.kscience:kmath-memory:0.4.2`.

**Gradle Kotlin DSL:**
```kotlin
repositories {
    maven("https://repo.kotlin.link")
    mavenCentral()
}

dependencies {
    implementation("space.kscience:kmath-memory:0.4.2")
}
```
