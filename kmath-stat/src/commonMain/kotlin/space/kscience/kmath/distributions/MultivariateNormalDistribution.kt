/*
 * Copyright 2018-2024 KMath contributors.
 * Use of this source code is governed by the Apache 2.0 license that can be found in the license/LICENSE.txt file.
 */

package space.kscience.kmath.distributions

public interface MultivariateNormalDistribution: NamedDistribution<Double> {
    public companion object{

    }
}